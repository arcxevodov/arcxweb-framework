<?php

namespace Egweb\Framework\Http;

use Egweb\Framework\Http\Exceptions\HttpException;
use Egweb\Framework\Router\RouterInterface;
use Exception;
use League\Container\Container;

class Kernel
{
    private string $appEnv;

    public function __construct(
        private readonly RouterInterface $router,
        private readonly Container $container
    ) {
        $this->appEnv = $this->container->get('APP_ENV');
    }

    public function handle(Request $request): Response
    {
        try {
            [$routeHandler, $params] = $this->router->dispatch($request, $this->container);
            $response = call_user_func_array($routeHandler, $params);
        } catch (Exception $exception) {
            $response = $this->createExceptionResponse($exception);
        }

        return $response;
    }

    private function createExceptionResponse(Exception $e): Response
    {
        if ($this->appEnv === "production") {
            return new Response('<h1 style="text-align: center">Internal Server Error</h1>', 500);
        } else {
            if ($e instanceof HttpException) {
                return new Response($e->getMessage(), $e->getStatusCode());
            }
            return new Response($e->getMessage(), 500);
        }
    }
}