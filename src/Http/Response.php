<?php

namespace Egweb\Framework\Http;

class Response
{
    public function __construct(
        private mixed $content = '',
        private int $status = 200,
        private array $headers = []
    ) {
        http_response_code($this->status);
    }

    public function send(): void
    {
        echo $this->content;
    }

    public function setContent(string $content): Response
    {
        $this->content = $content;
        return $this;
    }
}