<?php

namespace Egweb\Framework\Http;

class Request
{
    public function __construct(
        private readonly array $get,
        private readonly array $post,
        private readonly array $cookies,
        private readonly array $files,
        private readonly array $server,
    ) {
    }

    public static function createFromGlobals(): static
    {
        return new static($_GET, $_POST, $_COOKIE, $_FILES, $_SERVER);
    }

    public function getServerParam(string $method): string|false
    {
        if (!empty($this->server[$method])) {
            return explode('?', $this->server[$method])[0];
        }
        return false;
    }
}