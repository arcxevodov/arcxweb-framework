<?php

namespace Egweb\Framework\Controller;

use Egweb\Framework\Http\Response;
use Egweb\Telegram\Bot;
use Psr\Container\ContainerExceptionInterface;
use Psr\Container\ContainerInterface;
use Psr\Container\NotFoundExceptionInterface;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Error\SyntaxError;

abstract class AbstractController
{
    protected ?ContainerInterface $container = null;
    public ?Bot $botActions = null;

    /**
     * @throws ContainerExceptionInterface
     * @throws NotFoundExceptionInterface
     */
    public function setContainer(ContainerInterface $container): void
    {
        $this->container = $container;
        try {
            $this->botActions = $this->container->get('bot');
        } catch (NotFoundExceptionInterface) {
            $this->botActions = null;
        }
    }

    /**
     * @throws ContainerExceptionInterface
     * @throws NotFoundExceptionInterface
     * @throws LoaderError
     * @throws SyntaxError
     * @throws RuntimeError
     */
    public function render(string $view, array $params = [], Response $response = null): Response
    {
        $content = $this->container->get('twig')->render($view, $params);
        $response ??= new Response();
        $response->setContent($content);
        return $response;
    }
}