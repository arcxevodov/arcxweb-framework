<?php

namespace Egweb\Framework\Router;

use Egweb\Framework\Http\Exceptions\MethodNotAllowedException;
use Egweb\Framework\Http\Exceptions\NotFoundException;
use Egweb\Framework\Http\Request;
use FastRoute\Dispatcher;
use FastRoute\RouteCollector;
use League\Container\Container;

use function FastRoute\simpleDispatcher;

class Router implements RouterInterface
{
    public array $routes;

    /**
     * @throws MethodNotAllowedException
     * @throws NotFoundException
     */
    public function dispatch(Request $request, Container $container): array
    {
        [$handler, $params] = $this->extractRouteInfo($request);

        if (is_array($handler)) {
            [$controllerId, $method] = $handler;
            $controller = $container->get($controllerId);
            $handler = [$controller, $method];
        }

        return [$handler, $params];
    }

    /**
     * @throws MethodNotAllowedException
     * @throws NotFoundException
     */
    private function extractRouteInfo(Request $request): array
    {
        $dispatcher = simpleDispatcher(function (RouteCollector $collector) {
            foreach ($this->routes as $route) {
                $collector->addRoute(...$route);
            }
        });

        $routeInfo = $dispatcher->dispatch(
            $request->getServerParam('REQUEST_METHOD'),
            $request->getServerParam('REQUEST_URI')
        );

        switch ($routeInfo[0]) {
            case Dispatcher::FOUND:
                return [$routeInfo[1], $routeInfo[2]];
            case Dispatcher::METHOD_NOT_ALLOWED:
                $exception = new MethodNotAllowedException('Method Not Allowed');
                $exception->setStatusCode(405);
                throw $exception;
            default:
                $exception = new NotFoundException('Not Found');
                $exception->setStatusCode(404);
                throw $exception;
        }
    }

    public function registerRoutes(array $routes): void
    {
        $this->routes = $routes;
    }
}