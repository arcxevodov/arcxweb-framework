<?php

namespace Egweb\Framework\Router;

use Egweb\Framework\Http\Request;
use League\Container\Container;

interface RouterInterface
{
    public function dispatch(Request $request, Container $container);

    public function registerRoutes(array $routes);
}