<?php

namespace Egweb\Framework\Router;

class Route
{
    public static function get(string $uri, array|callable $handler): array
    {
        return ['GET', $uri, $handler];
    }

    public static function post(string $uri, array|callable $handler): array
    {
        return ['POST', $uri, $handler];
    }

    public static function put(string $uri, array|callable $handler): array
    {
        return ['PUT', $uri, $handler];
    }

    public static function delete(string $uri, array|callable $handler): array
    {
        return ['DELETE', $uri, $handler];
    }

    public static function patch(string $uri, array|callable $handler): array
    {
        return ['PATCH', $uri, $handler];
    }
}